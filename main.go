package main

import (
  "github.com/kataras/iris"
)

func newApp() *iris.Application {
  app := iris.Default()
  app.Get("/ping", getPing)
  return app
}

func main() {
  app := newApp()
  app.Run(iris.Addr(":8080"))
}

