package main

import (
  "github.com/kataras/iris"
)

func getPing(ctx iris.Context) {
  ctx.JSON(iris.Map{
    "message": "pong",
  })
}
