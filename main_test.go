package main

import (
  // "fmt"
  "testing"
  // "encoding/json"
  "github.com/kataras/iris/httptest"
)

type PingResponse struct {
  message string
}

func TestNewApp(t *testing.T) {
  app := newApp()
  e := httptest.New(t, app)
  e.GET("/").Expect().Status(httptest.StatusNotFound)
  e.GET("/randomUrl").Expect().Status(httptest.StatusNotFound)
  e.GET("/ping").Expect().Status(httptest.StatusOK).JSON()
}
