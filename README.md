# Go Tres en Raya

"Tres en Raya" or "noughts and crosses" implementation in Go Lang.

# todo 

01 [*] install golang environment on my local environment
02 [*] add simple http express like framework : iris
03 [*] add test suite framework 
04 [*] add nodemon like tool to watch and recompile code
05 [ ] write first test on the tres en raya


# notes

01 > 
via [gvm](https://github.com/moovweb/gvm), 
looks to be quite similar to [nvm](https://github.com/creationix/nvm)

02 > 
I made a quick look at simple http frameworks out there, and came up with
(iris)[https://github.com/kataras/iris]

The below command had to be run to install dependency: 
```go get github.com/kataras/iris```



03 > 
According to IRIS web framework documentation, we're going to focus on testing the http layer, using [HTTPTest](https://iris-go.com/v10/recipe#Testing)

To install the dependency, again with go get:
```go get github.com/kataras/iris/httptest```

Test, apparently are over the same filename with "_test." appended, 
i.e. main_test.go for main.go.

Visual Code Studio allows me to run the test directly from the IDE, 
clicking on the function, which is cool.

04 > 
I found a node cli module called (gomon)[https://github.com/johannesboyne/gomon]

